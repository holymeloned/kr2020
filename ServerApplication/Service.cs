﻿using interfaces;
using System.ServiceModel;
using System.Collections.Concurrent;
using Interfaces;
using System;
using System.Collections.Generic;

namespace ServerApplication
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)]
    public class Service : IService
    {
        //список користувачів онлайн
        public ConcurrentDictionary<string, ConnectedClient> _connectedClients = new ConcurrentDictionary<string, ConnectedClient>();


        int IService.Login(string userName)
        {
            //додавання користувача до списка онлайн
            //Перевірка по імені(оригінальність імені) серед підключених користувачів
            foreach(var client in _connectedClients)
            {
                if(client.Key.ToLower() == userName.ToLower())
                {
                    return 1;// помилка, таке ім'я вже зареєстроване
                }
            }
            var establishedUserConnection = OperationContext.Current.GetCallbackChannel<IClient>();

            ConnectedClient newClient = new ConnectedClient();
            newClient.connection = establishedUserConnection;
            newClient.UserName = userName;


            _connectedClients.TryAdd(userName, newClient);


            updateHelper(0, userName);

            //Повідомлення на сервері про нового користувача
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(DateTime.Now + ">> ###ServerMessage### Client: '" + newClient.UserName + "' connected to server");
            Console.ResetColor();

            return 0;//користувач успішно доданий
        }

        public void SendMessageToAll(string message, string userName)
        {
            foreach(var client in _connectedClients)
            {
                if (client.Key.ToLower() != userName.ToLower())
                {
                    //відправка повідомлення всім користувачам крім себе
                    client.Value.connection.GetMessage(message, userName);
                }
            }
        }

        public void Logout()
        {
            ConnectedClient client = GetMyClient();
            if(client != null)
            {
                ConnectedClient removedClient;
                _connectedClients.TryRemove(client.UserName, out removedClient);

                updateHelper(1, removedClient.UserName);

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(DateTime.Now + ">> ###ServerMessage### Client: '" + removedClient.UserName + "' leaved the server");
                Console.ResetColor();
            }
        }
        public ConnectedClient GetMyClient()
        {
            var establishedUserConnection = OperationContext.Current.GetCallbackChannel<IClient>();
            foreach(var client in _connectedClients)
            {
                if(client.Value.connection == establishedUserConnection)
                {
                    return client.Value;
                }
            }
            return null;
        }
        private void updateHelper(int value, string userName)
        {
            foreach (var client in _connectedClients)
            {
                if (client.Value.UserName.ToLower() != userName.ToLower())
                {
                    client.Value.connection.GetUpdate(value, userName);
                }
            }
        }

        public List<string> GetCurrentUsers()
        {
            List<string> listOfUsers = new List<string>();
            foreach(var client in _connectedClients)
            {
                listOfUsers.Add(client.Value.UserName);
            }
            return listOfUsers;
        }
    }
}