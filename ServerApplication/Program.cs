﻿using System;
using System.ServiceModel;

namespace ServerApplication
{
    class Program
    {
        public static Service _server;
        static void Main(string[] args)
        {
            _server = new Service();
            using(ServiceHost host = new ServiceHost(_server))
            {
                host.Open();
                Console.WriteLine(DateTime.Now + ">> ###ServerMessage### " + "Maraphone Server v. 2.0 started.");
                Console.ReadLine();
            }
        }
    }
}
