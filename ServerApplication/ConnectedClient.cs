﻿using Interfaces;

namespace ServerApplication
{
    public class ConnectedClient
    {
        public IClient connection;
        public string UserName { get; set; }
    }
}
