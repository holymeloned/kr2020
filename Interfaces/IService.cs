﻿using Interfaces;
using System.Collections.Generic;
using System.ServiceModel;

namespace interfaces
{
    [ServiceContract(CallbackContract =typeof(IClient))]
    public interface IService
    {
        [OperationContract]
        int Login(string userName);
        [OperationContract]
        void Logout();
        [OperationContract]
        void SendMessageToAll(string message, string userName);
        [OperationContract]
        List<string> GetCurrentUsers();
    }
}
