﻿using System.ServiceModel;

namespace Interfaces
{
    public interface IClient
        //Список операцій які сервер може викликати у клієнта
    {
        [OperationContract]
        //Отримати повідомлення
        void GetMessage(string message, string userName);
        [OperationContract]
        //статус користувачів для відображення 
        void GetUpdate(int value, string userName);
    }
}
