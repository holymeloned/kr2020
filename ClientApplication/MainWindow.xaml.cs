﻿using interfaces;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ClientApplication
{
    public partial class MainWindow : Window
    {
        public static IService Server;
        private static DuplexChannelFactory<IService> _channelfactory;
        public MainWindow()
        {
            InitializeComponent();
            _channelfactory = new DuplexChannelFactory<IService>(new ClientCallback(), "MaraphoneServiceEndPoint");
            Server = _channelfactory.CreateChannel();


        }
        public void TakeMessage(string message, string userName)
        {
            //відображення повідомлення
            TextDisplayTextBox.Text += userName + ": " + message + "\n";
            //автоматичний скроллінг чату
            TextDisplayTextBox.ScrollToEnd();
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageTextBox.Text.Length == 0)
            {
                MessageBox.Show("Введіть текст повідомлення");
                return;
            }
            //відправка повідомлення
            Server.SendMessageToAll(MessageTextBox.Text, UsrNameTextBox.Text);
            TakeMessage(MessageTextBox.Text, "Я");
            MessageTextBox.Text = "";
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            int returnValue = Server.Login(UsrNameTextBox.Text);
            if (returnValue == 1)
            {
                MessageBox.Show("Користувач під таким іменем вже зареєстрований");
            }
            else
            {
                MessageBox.Show("Ви увійшли");
                UsrNameTextBox.IsEnabled = false;
                LoginButton.IsEnabled = false;
                UsrNameTextBox.Width = 0;
                LoginButton.Width = 0;

                //Завантаження вже активних користувачів до списку користувачів
                LoadUserList(Server.GetCurrentUsers());
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Server.Logout();
        }
        public void AddUserToList(string userName)
        {
            if (UserListBox.Items.Contains(userName))
            {
                return;
            }
            UserListBox.Items.Add(userName);
        }
        public void RemoveUserFromList(string userName)
        {
            if (UserListBox.Items.Contains(userName))
            {
                UserListBox.Items.Remove(userName);
            }
        }
        private void LoadUserList(List<string> users)
        {
            foreach (var user in users)
            {
                AddUserToList(user);
            }
        }

        private void TextDisplayTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void LTButton_Click(object sender, RoutedEventArgs e)
        {
            LTRecMain.Opacity = 1;
        }

        private void DTButton_Click(object sender, RoutedEventArgs e)
        {
            LTRecMain.Opacity = 0;
        }

        private void ShowMenu(object sender, RoutedEventArgs e)
        {
            SettingsMenu.Width = 207;
        }
        private void NotShowMenu(object sender, RoutedEventArgs e)
        {
            SettingsMenu.Width = 0;
        }

    }
}